{-# LANGUAGE TypeFamilies #-}

-- shamelessly copied from http://stackoverflow.com/questions/20870432/type-family-vs-data-family-in-brief

import qualified Data.ByteString as S
import qualified Data.ByteString.Lazy as L
import Data.Word

type family Element container

type instance Element S.ByteString = Word8
type instance Element L.ByteString = Word8

data family XList a

data instance XList Char = XCons Char (XList Char) | XNil
data instance XList () = XListUnit Int
